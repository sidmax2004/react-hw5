import React from 'react';
import './Header.scss';
import PropTypes from "prop-types";

export default function Header() {

    const fav = JSON.parse(localStorage.getItem('favourites')).length;
    const cart = JSON.parse(localStorage.getItem('cart')).length;

    return (
        <div className='header-container'>
            <div className='header-chart'>{cart} items in cart</div>
            <div className='header-favourites'>{fav} items in favourites</div>
        </div>
    );
}

Header.propTypes = {
    cartArray: PropTypes.array,
    favouritesArray: PropTypes.array
}
