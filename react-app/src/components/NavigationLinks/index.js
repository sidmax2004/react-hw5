import {Link} from "react-router-dom";

export default function NavigationLinks(){

    return(
        <div className="links-container">
            <Link to={`/`}>Home |</Link>
            <Link to={`/favourites/`}>Favourites |</Link>
            <Link to={`/cart/`}>Cart</Link>
        </div>
    );
}
