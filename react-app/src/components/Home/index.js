import React from 'react';
import ProductList from "../ProductList";
import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {productListAction, productListError, productListLoading} from "../../redux/actions/productListAction";
import Header from "../Header";

export default function Home() {

    const [favourites, setFavourites] = useState([]);
    const [cart, setCart] = useState([]);
    const productList = useSelector(state => state.productsListReducer.products);
    const loading = useSelector(state => state.productsListReducer.isLoading);
    const errored = useSelector(state => state.productsListReducer.errored);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(productListAction());
        if (!productList) {
            dispatch(productListLoading(true))
        } else {
            dispatch(productListLoading(false));
        }
    }, [dispatch])

    useEffect(()=>{
        if (productList.length === 0) {
            dispatch(productListError(true))
        } else {
            dispatch(productListError(false))
        }
    }, [dispatch, productList])

    return (
        <div>
            <Header/>
            {loading
                ? <h1>The content is loading...</h1>
                : errored
                    ? <h1>Something went wrong :'(</h1>
                : <ProductList items={productList}
                               cart={cart}
                               setCart={setCart}
                               favourites={favourites}
                               setFavourites={setFavourites}
                />
            }
        </div>
    );
}
