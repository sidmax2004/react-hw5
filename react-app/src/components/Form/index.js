import React from 'react';
import {Formik, Form, Field} from 'formik';
import * as Yup from 'yup';
import './styles.scss';
import {useDispatch} from 'react-redux';
import {cartSubmit} from "../../redux/actions/cartAction";

const validationScheme = Yup.object().shape({
    firstName: Yup.string()
        .required("Required")
        .min(2, 'Name must contain at least 2 characters'),

    lastName: Yup.string()
        .required("Required")
        .min(2, 'Name must contain at least 2 characters'),

    age: Yup.number()
        .required("Required")
        .positive()
        .integer(),

    phoneNumber: Yup.number()
        .required("Required")
        .positive()
        .integer(),

    address: Yup.string()
        .required("Required")
        .min(10, 'Address must contain at least city and street'),
});

export default function FormForPurchase({show, onClose, setCart, cart}){
    const dispatch = useDispatch();
    const onSubmitForm = (data, {resetForm}) =>{
        console.log('form submitted', data);
        resetForm({data: ''});
        dispatch(cartSubmit());
        setCart([]);
        onClose();
    }

    const onCloseArea = (e) => {
        if (e.target.className === 'container-for-form') {
            onClose()
        }
    }

    const style = {
        display: show? 'flex' : 'none'
    }

    return(
        <div className='container-for-form' style={style} onClick={onCloseArea}>
            <Formik
                className='formik-container'
                initialValues={{
                    firstName: '',
                    lastName: '',
                    age: '',
                    phoneNumber: '',
                    address: ''
                }}
                validationSchema={validationScheme}
                onSubmit={onSubmitForm}
            >
                {
                    ({errors, touched}) => (
                        <Form className='form-container'>
                            <label>First Name</label>
                            <Field name="firstName"
                                   type="text"
                                   placeholder="e.x. Jack"/>
                            {errors.firstName && touched.firstName ? (
                                <div className='form-input-error'>{errors.firstName}</div>
                            ) : null}
                            <label>Last Name</label>
                            <Field name="lastName"
                                   type="text"
                                   placeholder="e.x. McConnakhie"/>
                            {errors.lastName && touched.lastName ? (
                                <div className='form-input-error'>{errors.lastName}</div>
                            ) : null}
                            <label>Age</label>
                            <Field name="age"
                                   type="text"
                                   placeholder="e.x. 34"/>
                            {errors.age && touched.age ? (
                                <div className='form-input-error'>{errors.age}</div>
                            ) : null}
                            <label>Phone number</label>
                            <Field name="phoneNumber"
                                   type="text"
                                   placeholder="e.x. 380631234567"/>
                            {errors.phoneNumber && touched.phoneNumber ? (
                                <div className='form-input-error'>{errors.phoneNumber}</div>
                            ) : null}
                            <label>Address</label>
                            <Field name="address"
                                   type="text"
                                   placeholder="e.x. Ukraine, Kyiv, 72 Peremohy Ave."/>
                            {errors.address && touched.address ? (
                                <div className='form-input-error'>{errors.address}</div>
                            ) : null}
                            <div className="button-container">
                                <button className='button-submit' type="submit">Submit</button>
                            </div>
                        </Form>
                    )
                }
            </Formik>
        </div>
    );
}
