import React from 'react';
import ProductItem from "../ProductItem";
import {useDispatch, useSelector} from "react-redux";
import {useState} from 'react';
import {favouritesDeleteAction} from "../../redux/actions/favouritesAction";

export default function Favourites() {

    const favouritesInLS = JSON.parse(localStorage.getItem('favourites'));
    const [favourites, setFavourite] = useState([]);
    // const favouritesArrayRedux = useSelector(state => state.favouritesReducer.favourites);
    const dispatch = useDispatch();

    const deleteItem = (e) => {
        let product = JSON.parse(e.target.dataset.product)
        setFavourite([e.target.dataset.product]);
        const filtered = favouritesInLS.filter((item) => item.id !== product.id);
        dispatch(favouritesDeleteAction(product));
        setFavourite(filtered);
        localStorage.setItem('favourites', JSON.stringify(filtered));

    }


    return (
        <div>
            <h3>This is Favourites Page</h3>
            <h4>There are {favouritesInLS.length} items in favourites</h4>
            <div className='container'>
                {
                    favouritesInLS.map
                    (item =>
                        <div className='product-container'
                             key={JSON.stringify(item.id)}>
                            <ProductItem item={item} showStar={true} showCross={false}
                                         onClick={deleteItem}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}
