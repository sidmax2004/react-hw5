import React from 'react';
import PropTypes from 'prop-types';
import './ProductList.scss';
import '../Button/Button.scss'
import Button from "../Button";
import Modal from "../Modal";
import ProductItem from "../ProductItem";
import {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {modalHideAction, modalShowAction} from "../../redux/actions/modalAction";
import {favouritesAddAction} from "../../redux/actions/favouritesAction";
import {cartAddAction} from "../../redux/actions/cartAction";

export default function ProductList(props) {

    const [product, setProduct] = useState(null);
    const showModal = useSelector(state => state.modalReducer.show);
    const favouritesArrayRedux = useSelector(state => state.favouritesReducer.favourites);
    // const cartArrayRedux = useSelector(state => state.cartReducer.cart);
    const dispatch = useDispatch();

    const openModal = (e) =>{
        dispatch(modalShowAction());
        setProduct(e.target.dataset.cart);
    }

    const closeModal = () => {
        dispatch(modalHideAction());
    }

    const addToFavourites = (e) => {
        let clickedStar = e.target;
        let clickedProduct = JSON.parse(e.target.dataset.product);
        let index = favouritesArrayRedux.findIndex(item => item.id === clickedProduct.id);
        let clonedFavourites = [...props.favourites];
        if (index < 0) {
            clickedStar.classList.toggle('button-star-clicked');
            clonedFavourites.push(clickedProduct);
            dispatch(favouritesAddAction(clickedProduct));
            props.setFavourites([...clonedFavourites]);
            localStorage.setItem('favourites', JSON.stringify(clonedFavourites));
        }
    }

    const addToCart = (e) => {
        let product = JSON.parse(e.target.dataset.cart);
        let clonedCart = [...props.cart];
        clonedCart.push(product);
        props.setCart([...clonedCart]);
        dispatch(cartAddAction(product));
        localStorage.setItem('cart', JSON.stringify(clonedCart));
    }


    return (
        <div className='container'>
            <Modal show={showModal}
                   product={product}
                   onSuccessAdd={addToCart}
                   onClick={closeModal}
                   onClose={closeModal}
                   textHeader={'Are you sure you want to buy this?'}
                   textBody={'Add to cart'}
            />
            {
                props.items.map
                (item =>
                    <div className='product-container'
                         key={JSON.stringify(item.id)}>

                        <Button modification='add-to-chart'
                                onClick={openModal}
                                text='Add to cart'
                                toCart={JSON.stringify(item)}

                        />
                        <ProductItem item={item}
                                     onClick={addToFavourites}
                                     showStar={true}
                                     isActive={JSON.parse(localStorage.getItem('favourites')).find((elem) => elem.id === item.id)}
                                     showCross={false}
                        />
                    </div>
                )
            }
        </div>
    );
}

ProductList.propTypes = {
    items: PropTypes.array,
    onClick: PropTypes.func
}
