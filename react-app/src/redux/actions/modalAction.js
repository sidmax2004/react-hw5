export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';

export const modalShowAction = () => (dispatch) =>{

    dispatch({
        type: SHOW_MODAL,
        payload: true
    })
}

export const modalHideAction = () => (dispatch) =>{

    dispatch({
        type: HIDE_MODAL,
        payload: false
    })
}
