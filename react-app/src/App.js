import React from 'react';
import './App.css'
import Main from "./components/Main";
import NavigationLinks from "./components/NavigationLinks";

export function App() {

    console.log('---RENDER---');
    if (!localStorage.getItem('cart')){
        localStorage.setItem('cart','[]')
    }
    if (!localStorage.getItem('favourites')){
        localStorage.setItem('favourites','[]')
    }

    return (
        <div className="App">
            <NavigationLinks/>
            <Main/>
        </div>
    );
}
